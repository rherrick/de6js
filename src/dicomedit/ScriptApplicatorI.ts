import { DicomObjectI } from './DicomObjectI'

export interface ScriptApplicatorI {

    apply ( file: string): DicomObjectI

}
